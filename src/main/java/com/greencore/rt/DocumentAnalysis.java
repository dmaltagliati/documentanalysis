package com.greencore.rt;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.greencore.rt.data.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.util.*;


/**
 * Created by User on 10/10/2018.
 */
public class DocumentAnalysis {

    //public static HashMap<String , ErrorDescription> errDescMap = new HashMap<>();
    public static List<ErrorDescription> errDescList = new ArrayList<>();

    public static final String Versione = "1.7.001";
    public static void main(String[] args) {

        if (!Log4JUtil.init()){
            System.out.println(" log4jInit failed... >./conf/RTDocAnalysis_log4j.properties< not found exit program");
            System.exit(98);
        }

        Log4JUtil.get(DocumentAnalysis.class).fatal("----------------------------------------------------------");
        Log4JUtil.get(DocumentAnalysis.class).fatal("------ RTDocumentAnalysis release:  " + Versione);
        Log4JUtil.get(DocumentAnalysis.class).fatal("----------------------------------------------------------");

        createErrDescMap();

        String filename;
        if (args.length == 0){
            filename = ".";
        }else {
            filename = args[0];
            Log4JUtil.get(DocumentAnalysis.class).fatal("args[0]: " + args[0] + " - filename: "  + filename);
        }
        Log4JUtil.get(DocumentAnalysis.class).fatal("filename: "  + filename);

        File rtFile = new File(filename);
        List<File> rtFiles = new ArrayList<File>();
        File adaptFile = new File(filename);
        List<File> adaptFiles = new ArrayList<File>();

        Gson gson = new Gson();
        Map<String, Container> containers = new HashMap<String, Container>();
        Map<String, Response> errDescMap = new HashMap<>();
        Response respErr = new Response();

        try {
            Log4JUtil.get(DocumentAnalysis.class).info("aggiungo file da analizzare : ");
            if (rtFile.isDirectory()) {

                rtFiles = Arrays.asList(rtFile.listFiles((d, name) -> name.endsWith(".json")));
                //adaptFiles = Arrays.asList(adaptFile.listFiles((d, name) -> name.endsWith("_request.dat") || name.endsWith("_response.log"))  );
                adaptFiles = Arrays.asList(adaptFile.listFiles((d, name) -> name.endsWith("_request.dat") )  );

            } else {
                rtFiles.add(rtFile);
            }

            for (File singleFile : rtFiles) {
                Log4JUtil.get(DocumentAnalysis.class).info("Accodo file  : " + singleFile.getName());
                JsonReader reader = new JsonReader(new FileReader(singleFile));
                Container container = gson.fromJson(reader, RTContainer.class);
                containers.put(singleFile.getName(), container);
            }
            for (File singleFile : adaptFiles) {
                Log4JUtil.get(DocumentAnalysis.class).info("Accodo file                     : " + singleFile.getName());
                BufferedReader input = new BufferedReader(new FileReader(singleFile));
                StringBuffer buffer = new StringBuffer();
                StringBuffer bufferlog = new StringBuffer();
                String line;
                while((line = input.readLine()) != null){
                    if (line.startsWith("{")){
                        line = line.replace("\\\"", "\"");
                        line = line.replace("\"{", "{");
                        line = line.substring(0, line.lastIndexOf("\",\"qrData")) + "}";
                        buffer.append(line);
                    }
                }
                input.close();
                String responseLog = "";

                try {
                    responseLog = singleFile.getName().substring(0, singleFile.getName().lastIndexOf("_request.dat")) + "_response.log";
                    Log4JUtil.get(DocumentAnalysis.class).info("Cerco l'eventuale response.log  : " + responseLog);
                    input = new BufferedReader(new FileReader(filename + File.separator + responseLog));


                    while ((line = input.readLine()) != null) {
                        line = line.substring(line.indexOf("{"));
                        bufferlog.append(line);
                    }

                }catch (Exception e){
                    Log4JUtil.get(DocumentAnalysis.class).fatal("file >" + responseLog +"< non trovato");
                }

                //JsonReader reader = new JsonReader(new FileReader(singleFile));

                try {
                    respErr = gson.fromJson(bufferlog.toString(), Response.class);
                    errDescMap.put(singleFile.getName(), respErr);
                }catch (Exception e){
                    Log4JUtil.get(DocumentAnalysis.class).fatal("responseLog non elaborato");
                }

                Container container = gson.fromJson(buffer.toString(), AdapterContainer.class);
                containers.put(singleFile.getName(), container);
            }
            Log4JUtil.get(DocumentAnalysis.class).info("Analizzo i file accodati");
            ExcelWriter.getInstance().writeFile(containers, errDescMap);
        } catch (Exception e) {

            StringWriter stringWriter= new StringWriter();
            PrintWriter printWriter= new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            String stackTraceAsString= stringWriter.toString();


            Log4JUtil.get(DocumentAnalysis.class).fatal("Errore nell'elaborazione. e:" + e.getMessage());
            Log4JUtil.get(DocumentAnalysis.class).fatal(stackTraceAsString);
            System.out.println("Errore nell'elaborazione. e:" + e.getMessage());
            e.printStackTrace();
        }
    }

    public static void createErrDescMap(){
        Log4JUtil.get(DocumentAnalysis.class).info("Creo foglio LEGENDA");
        try {
            String line;
            String[] lineSplitted;
            ErrorDescription errDesc;
            BufferedReader input = new BufferedReader(new FileReader("./conf/RTServerWarnings.txt"));

            while((line = input.readLine()) != null){
                Log4JUtil.get(DocumentAnalysis.class).debug("line: " + line);
                lineSplitted = line.split(";");
                //Log4JUtil.get(DocumentAnalysis.class).debug("lineSplitted[0]: " + lineSplitted[0] + ", " + "lineSplitted[1]: " + lineSplitted[1] + ", " + "lineSplitted[2]: " + lineSplitted[2]);
                errDesc = new ErrorDescription(lineSplitted[0], lineSplitted[1], lineSplitted[2]);
                errDescList.add(errDesc);
            }

        }catch (Exception e){
            Log4JUtil.get(DocumentAnalysis.class).info("file ./conf/RTServerWarnings.txt non found. Exeption e: " + e.getMessage());
        }
    }

}
