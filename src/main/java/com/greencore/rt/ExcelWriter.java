package com.greencore.rt;

import com.greencore.rt.data.*;
import org.apache.poi.Version;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by User on 10/10/2018.
 */
public class ExcelWriter {
    public static int DESCRIZIONE = 0;
    public static int AMMONTARE = 1;
    public static int AMMONTARE_LORDO = 2;
    public static int ALIQ_IVA_ARTICOLO = 3;
    public static int CODICE_ESENZIONE_ITM = 4;
    public static int ALIQUOTA_IVA = 6;
    public static int CODICE_ESENZIONE_TOT = 7;
    public static int LORDO_IVA = 8;
    public static int IVA = 9;
    public static int LORDO_IVA_CALCOLATO = 10;
    public static int TOTALE_SCONTRINO = 12;
    public static int TOTALE_ARTICOLI = 13;
    public static int TOTALE_IVE = 14;

    private static String[] columns = {"Descrizione", "Ammontare", "Ammontare Lordo", "Al. IVA Articolo", "Cod.Ese.", "", "Aliquota IVA", "Cod.Ese.", "Lordo Iva", "IVA", "Lordo IVA Calcolato", "", "Tot. Scontrino", "Tot. Articoli", "Tot. Ive"};
    private static String[] columnsLegenda = {"COD.ERRORE", "NOME", "COMMENTO"};
    private static ExcelWriter instance = null;
    private static final String ITEM = "1";
    Workbook workbook = new XSSFWorkbook();

    private CellStyle amountCellStyle;
    private CellStyle errorAmountCellStyle;
    private CellStyle percentCellStyle;
    private CellStyle vatCodeCellStyle;

    private ExcelWriter() {
        amountCellStyle = createAmountCellStyle(false);
        errorAmountCellStyle = createAmountCellStyle(true);
        percentCellStyle = createPercentCellStyle();
    }

    public static ExcelWriter getInstance() {
        if (instance == null) {
            instance = new ExcelWriter();
        }
        return instance;
    }

    private CellStyle createHeaderCellStyle() {
        Font font = workbook.createFont();
        font.setBold(true);
        font.setFontHeightInPoints((short) 14);
        font.setColor(IndexedColors.BLUE.getIndex());
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFont(font);
        return cellStyle;
    }

    private CellStyle createAmountCellStyle(boolean error) {
        Font font = workbook.createFont();
        font.setFontHeightInPoints((short) 12);
        if (error) {
            font.setBold(true);
            font.setColor(IndexedColors.RED.getIndex());
        }
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFont(font);
        cellStyle.setDataFormat((short)8);
        return cellStyle;
    }

    private CellStyle createDateCellStyle() {
        CreationHelper createHelper = workbook.getCreationHelper();
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));
        return cellStyle;
    }

    private CellStyle createPercentCellStyle() {
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setDataFormat(workbook.createDataFormat().getFormat("0.00%"));
        return cellStyle;
    }

    private CellStyle createDescStyle(){
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        return cellStyle;

    }
    private CellStyle createNonItemCellStyle() {
        Font font = workbook.createFont();
        font.setFontHeightInPoints((short) 11);
        font.setColor(IndexedColors.GREY_25_PERCENT.getIndex());
        font.setFontName("Courier New");
        font.setBold(true);
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFont(font);
        return cellStyle;
    }

    private CellStyle createItemCellStyle() {
        Font font = workbook.createFont();
        font.setFontHeightInPoints((short) 11);
        font.setColor(IndexedColors.BLACK.getIndex());
        font.setFontName("Courier New");
        font.setBold(true);
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFont(font);
        return cellStyle;
    }

    private CellStyle createVatCodeCellStyle() {
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        return cellStyle;
    }

    private void createResultColums(Sheet sheet, Response response) {
        Log4JUtil.get(ExcelWriter.class).info("Creo RESULT COLUMN. ResponseCode: " + response.getResponseCode());
        CellStyle amountCellStyle = createAmountCellStyle(false);
        int rowNum = 1;
        //Row row = sheet.createRow(rowNum);
        Row row = sheet.getRow(rowNum);

        String cellValue, motivo = "";
        String siNo = "N.D.";

        if (response.isFromAdapter()) {

            if (response.getResponseCode() != null) {
                if (response.getResponseCode().equals("0")) {
                    siNo = "NO";
                } else {
                    siNo = "SI - ERRORE: " + response.getResponseCode();
                }
            }
        }else{
            if (response.getDoctype().equals("1") || response.getDoctype().equals("2") || response.getDoctype().equals("3")){
                siNo = "NO";
            }
            else{
                siNo = "SI - ERRORE: N.D.";
            }
        }
        cellValue = "RIGETTATO : " + siNo;

        Cell cell = row.createCell(DESCRIZIONE);
        cell.setCellStyle(createAmountCellStyle(siNo.indexOf("SI") >= 0));
        cell.setCellValue(cellValue);

//        if (siNo.equals("SI")) {
//            rowNum = 2;
//
//            //row = sheet.createRow(rowNum);
//            row = sheet.getRow(rowNum);
//            cell = row.createCell(DESCRIZIONE);
//            cell.setCellStyle(createAmountCellStyle(false));
//            cell.setCellValue(motivo);
//        }


    }
    private void createItemColumns(Sheet sheet, Container container) {
        Log4JUtil.get(ExcelWriter.class).info("Creo ITEM COLUMN");
        CellStyle amountCellStyle = createAmountCellStyle(false);
        CellStyle percentCellStyle = createPercentCellStyle();
        Log4JUtil.get(ExcelWriter.class).info("Creo ITEM COLUMN");
        int rowNum = 1;
        Row row = sheet.createRow(rowNum++);
        Cell cell = row.createCell(DESCRIZIONE);
        cell.setCellStyle(createItemCellStyle());
        cell.setCellValue(" ");

        for(Item item: container.getData().getItems()) {
            Log4JUtil.get(ExcelWriter.class).debug("item.getDescription(): " + item.getDescription());
            if (ITEM.equals(item.getType())) {
                sumAmounts(item, container);
            }

            row = sheet.createRow(rowNum++);
            cell = row.createCell(DESCRIZIONE);
            cell.setCellStyle(createItemCellStyle());
            cell.setCellValue(item.getDescription());

            if (!ITEM.equals(item.getType())) {
                cell.setCellStyle(createNonItemCellStyle());
                continue;
            }

            cell = row.createCell(AMMONTARE);
            cell.setCellStyle(amountCellStyle);
            cell.setCellValue(Double.parseDouble(item.getAmount()) / 100);

            cell = row.createCell(AMMONTARE_LORDO);
            cell.setCellStyle(amountCellStyle);
            cell.setCellValue(Double.parseDouble(item.getAmountOriginal()) / 100);

            cell = row.createCell(ALIQ_IVA_ARTICOLO);
            cell.setCellStyle(percentCellStyle);
            cell.setCellValue(Double.parseDouble(item.getVatValue()) / 10000);

            cell = row.createCell(CODICE_ESENZIONE_ITM);
            cell.setCellStyle(createVatCodeCellStyle());
            cell.setCellValue(item.getVatCode());

        }
    }

    private void sumAmounts(Item item, Container container) {
        Log4JUtil.get(ExcelWriter.class).info("******* sumAmounts *******");
        Data data = container.getData();

        data.getDocument().setItemsSum(data.getDocument().getItemsSum() + Double.valueOf(item.getAmount()) / 100);
        for(Tax tax : data.getTaxes()) {
            Log4JUtil.get(ExcelWriter.class).debug(" - - - - - - - - - - - - - - - - -");
            Log4JUtil.get(ExcelWriter.class).debug("item.getVatValue(): >" + item.getVatValue() + "< --equals-- tax.getVatValue(): >" + tax.getVatValue() + "< &&");
            Log4JUtil.get(ExcelWriter.class).debug(" item.getVatCode(): >" + item.getVatCode() + "< --equals-- tax.getVatCode(): >"+ tax.getVatCode() + "<");
            //if (item.getVatValue().equals(tax.getVatValue())) {
            if (item.getVatValue().equals(tax.getVatValue()) && item.getVatCode().equals(tax.getVatCode())) {
                Log4JUtil.get(ExcelWriter.class).debug("tax.getItemsSum() + Double.valueOf(item.getAmount()): " + tax.getItemsSum() +" , "+ Double.valueOf(item.getAmount()));
                tax.setItemsSum(tax.getItemsSum() + Double.valueOf(item.getAmount()) / 100);
                return;
            }
        }
    }

    private void createVatColumns(Sheet sheet, Container container) {
        Log4JUtil.get(ExcelWriter.class).info("Creo VAT COLUMN");
        Data data = container.getData();

        int rowNum = 1;
        for(Tax tax: container.getData().getTaxes()) {
            Log4JUtil.get(ExcelWriter.class).debug("createVatColumns. gross: " + tax.getGross() + "; tax: " + tax.getTax());
            data.getDocument().setTaxesSum(data.getDocument().getTaxesSum() + Double.valueOf(tax.getGross()) / 100);

            rowNum++;
            Log4JUtil.get(ExcelWriter.class).debug("rowNum: " + rowNum);

            Row row = sheet.getRow(rowNum);
            if (row == null) {
                row = sheet.createRow(rowNum);
            }
            Cell cell = row.createCell(ALIQUOTA_IVA);
            cell.setCellStyle(percentCellStyle);
            cell.setCellValue(Double.parseDouble(tax.getVatValue()) / 10000);

            cell = row.createCell(CODICE_ESENZIONE_TOT);
            cell.setCellStyle(createVatCodeCellStyle());
            cell.setCellValue(tax.getVatCode());

            double taxAmount = Double.parseDouble(tax.getGross()) / 100;
            cell = row.createCell(LORDO_IVA);
            cell.setCellStyle(amountCellStyle);
            if (Math.round(tax.getItemsSum() * 100) != Math.round(taxAmount * 100)) {
                cell.setCellStyle(errorAmountCellStyle);
            }
            cell.setCellValue(taxAmount);

            cell = row.createCell(IVA);
            cell.setCellStyle(amountCellStyle);
            cell.setCellValue(Double.parseDouble(tax.getTax()) / 100);

            cell = row.createCell(LORDO_IVA_CALCOLATO);
            cell.setCellStyle(amountCellStyle);
            if (Math.round(tax.getItemsSum() * 100) != Math.round(taxAmount * 100)) {
                cell.setCellStyle(errorAmountCellStyle);
            }
            cell.setCellValue(tax.getItemsSum());
        }
    }

    private void createTotalColumns(Sheet sheet, Container container) {
        Log4JUtil.get(ExcelWriter.class).info("Creo TOTAL COLUMN");
        Row row = sheet.getRow(1);

        double totalAmount = Double.parseDouble(container.getData().getDocument().getAmount()) / 100;
        double itemsSum = container.getData().getDocument().getItemsSum();
        double taxesSum = container.getData().getDocument().getTaxesSum();

        Cell cell = row.createCell(TOTALE_SCONTRINO);
        cell.setCellStyle(amountCellStyle);
        if (Math.round(itemsSum * 100) != Math.round(totalAmount * 100) || Math.round(taxesSum * 100) != Math.round(totalAmount * 100)) {
            cell.setCellStyle(errorAmountCellStyle);
        }
        cell.setCellValue(totalAmount);

        cell = row.createCell(TOTALE_ARTICOLI);
        cell.setCellStyle(amountCellStyle);
        if (Math.round(itemsSum * 100) != Math.round(totalAmount * 100)) {
            cell.setCellStyle(errorAmountCellStyle);
        }
        cell.setCellValue(itemsSum);

        cell = row.createCell(TOTALE_IVE);
        cell.setCellStyle(amountCellStyle);
        if (Math.round(taxesSum * 100) != Math.round(totalAmount * 100)) {
            cell.setCellStyle(errorAmountCellStyle);
        }
        cell.setCellValue(taxesSum);
    }

    //public void writeFile(Map<String, Container> containers) throws IOException, InvalidFormatException {
    public void writeFile(Map<String, Container> containers, Map<String, Response> responseMap) throws IOException, InvalidFormatException {
        createAboutSheet();
        createLegendaSheet();
        for (String filename : containers.keySet()) {
            String sheetName;
            int beg = filename.indexOf("insertFiscalDocument_") + "insertFiscalDocument_".length();
            int end = filename.indexOf("_request.dat");
            try {
                sheetName = filename.substring(beg, end);
            }catch (Exception e){
                sheetName = filename;
            }
            Sheet sheet = workbook.createSheet(sheetName);
            Log4JUtil.get(ExcelWriter.class).info("sheet.getSheetName(): " + sheet.getSheetName());
            sheet.getSheetName();
            Row headerRow = sheet.createRow(0);
            CellStyle headerCellStyle = createHeaderCellStyle();

            for(int i = 0; i < columns.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(columns[i]);
                cell.setCellStyle(headerCellStyle);
                sheet.autoSizeColumn(i, true);
            }


            createItemColumns(sheet, containers.get(filename));
            createVatColumns(sheet, containers.get(filename));
            createTotalColumns(sheet, containers.get(filename));

            Response response = responseMap.get(filename);

            if (response == null) {
                response = new Response();
                if (containers.get(filename) instanceof AdapterContainer)
                    response.setFromAdapter(true);
                else{
                    response.setFromAdapter(false);
                    response.setDoctype(containers.get(filename).getData().getDocument().getType());
                }
            }
            createResultColums(sheet, response);

            for(int i = 0; i < columns.length; i++) {
                sheet.autoSizeColumn(i);
            }
        }

        FileOutputStream fileOut = new FileOutputStream("RTDocumentAnalyzed.xlsx");
        workbook.setActiveSheet(2);
        workbook.write(fileOut);
        fileOut.close();

        workbook.close();
    }

    public  void createLegendaSheet(){
        Sheet sheet = workbook.createSheet("Legenda");
        Row headerRow = sheet.createRow(0);
        CellStyle headerCellStyle = createHeaderCellStyle();
        ErrorDescription ed;

        for(int i = 0; i < columnsLegenda.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columnsLegenda[i]);
            cell.setCellStyle(headerCellStyle);
            sheet.autoSizeColumn(i, true);
        }

        int rowNum = 1;
        Iterator<ErrorDescription> it = DocumentAnalysis.errDescList.iterator();
        while (it.hasNext()){
            ed = it.next();
            Row row = sheet.createRow(rowNum++);

            Cell cell = row.createCell(0);
            cell.setCellStyle(createAmountCellStyle(false));
            cell.setCellValue(ed.getErrorCode());

            cell = row.createCell(1);
            cell.setCellStyle(createAmountCellStyle(false));
            cell.setCellValue(ed.getName());
            sheet.autoSizeColumn(1, true);

            cell = row.createCell(2);
            cell.setCellStyle(createAmountCellStyle(false));
            cell.setCellValue(ed.getDescription());
        }
    }

    public void createAboutSheet() {
        Sheet sheet = workbook.createSheet("about");
        Row headerRow = sheet.createRow(0);
        CellStyle headerCellStyle = createHeaderCellStyle();
        ErrorDescription ed;


        Cell cell = headerRow.createCell(1);
        cell.setCellValue("Versione: " + DocumentAnalysis.Versione);
        cell.setCellStyle(headerCellStyle);
        sheet.autoSizeColumn(1, true);

        headerRow = sheet.createRow(1);
        cell = headerRow.createCell(1);
        cell.setCellValue("by GREENCORE srl");
        cell.setCellStyle(headerCellStyle);
        sheet.autoSizeColumn(1, true);

    }

}
