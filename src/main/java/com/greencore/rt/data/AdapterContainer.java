package com.greencore.rt.data;

public class AdapterContainer implements Container {
    private Data fiscalData;


    public Data getData() {
        return fiscalData;
    }

    public void setData(Data fiscalData) {
        this.fiscalData = fiscalData;
    }
    public Data getfiscalDataData() {
        return fiscalData;
    }

    public void setfiscalDataData(Data fiscalData) {
        this.fiscalData = fiscalData;
    }

}
