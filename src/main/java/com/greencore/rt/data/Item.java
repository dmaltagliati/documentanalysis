package com.greencore.rt.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 10/10/2018.
 */
public class Item {
    private String amount;
    @SerializedName("amount_original")
    private String amountOriginal;
    private String department;
    private String description;
    @SerializedName("fiscalvoid")
    private String fiscalVoid;
    @SerializedName("paymentid")
    private String paymentId;
    private String plu;
    private String quantity;
    @SerializedName("signid")
    private String signId;
    private String type;
    @SerializedName("unitprice")
    private String unitPrice;
    @SerializedName("vatcode")
    private String vatCode;
    @SerializedName("vatvalue")
    private String vatValue;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAmountOriginal() {
        return amountOriginal;
    }

    public void setAmountOriginal(String amountOriginal) {
        this.amountOriginal = amountOriginal;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFiscalVoid() {
        return fiscalVoid;
    }

    public void setFiscalVoid(String fiscalVoid) {
        this.fiscalVoid = fiscalVoid;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getPlu() {
        return plu;
    }

    public void setPlu(String plu) {
        this.plu = plu;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getSignId() {
        return signId;
    }

    public void setSignId(String signId) {
        this.signId = signId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getVatCode() {
        if (vatCode == null)
            vatCode = "";
        return vatCode;
    }

    public void setVatCode(String vatCode) {
        this.vatCode = vatCode;
    }

    public String getVatValue() {
        return vatValue;
    }

    public void setVatValue(String vatValue) {
        this.vatValue = vatValue;
    }
}
