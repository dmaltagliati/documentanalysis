package com.greencore.rt.data;

public class ResponseError {
    String err_fm_present;
    String err_ej_present;
    String err_mkey_present;
    String err_mkey_valid;
    String err_ej_full;
    String err_fm_full;
    String err_hwinit_max;
    String err_cert_expired;
    String err_count;
    String warn_ej_full;
    String warn_fm_full;
    String warn_hwinit_max;
    String warn_cert_expired;
    String warn_count;
    String warn_hwinit_val;
    String warn_fm_full_val;
    String warn_ej_full_val;
    String err_fm_status;

    public String getErr_fm_present() {
        return err_fm_present;
    }

    public void setErr_fm_present(String err_fm_present) {
        this.err_fm_present = err_fm_present;
    }

    public String getErr_ej_present() {
        return err_ej_present;
    }

    public void setErr_ej_present(String err_ej_present) {
        this.err_ej_present = err_ej_present;
    }

    public String getErr_mkey_present() {
        return err_mkey_present;
    }

    public void setErr_mkey_present(String err_mkey_present) {
        this.err_mkey_present = err_mkey_present;
    }

    public String getErr_mkey_valid() {
        return err_mkey_valid;
    }

    public void setErr_mkey_valid(String err_mkey_valid) {
        this.err_mkey_valid = err_mkey_valid;
    }

    public String getErr_ej_full() {
        return err_ej_full;
    }

    public void setErr_ej_full(String err_ej_full) {
        this.err_ej_full = err_ej_full;
    }

    public String getErr_fm_full() {
        return err_fm_full;
    }

    public void setErr_fm_full(String err_fm_full) {
        this.err_fm_full = err_fm_full;
    }

    public String getErr_hwinit_max() {
        return err_hwinit_max;
    }

    public void setErr_hwinit_max(String err_hwinit_max) {
        this.err_hwinit_max = err_hwinit_max;
    }

    public String getErr_cert_expired() {
        return err_cert_expired;
    }

    public void setErr_cert_expired(String err_cert_expired) {
        this.err_cert_expired = err_cert_expired;
    }

    public String getErr_count() {
        return err_count;
    }

    public void setErr_count(String err_count) {
        this.err_count = err_count;
    }

    public String getWarn_ej_full() {
        return warn_ej_full;
    }

    public void setWarn_ej_full(String warn_ej_full) {
        this.warn_ej_full = warn_ej_full;
    }

    public String getWarn_fm_full() {
        return warn_fm_full;
    }

    public void setWarn_fm_full(String warn_fm_full) {
        this.warn_fm_full = warn_fm_full;
    }

    public String getWarn_hwinit_max() {
        return warn_hwinit_max;
    }

    public void setWarn_hwinit_max(String warn_hwinit_max) {
        this.warn_hwinit_max = warn_hwinit_max;
    }

    public String getWarn_cert_expired() {
        return warn_cert_expired;
    }

    public void setWarn_cert_expired(String warn_cert_expired) {
        this.warn_cert_expired = warn_cert_expired;
    }

    public String getWarn_count() {
        return warn_count;
    }

    public void setWarn_count(String warn_count) {
        this.warn_count = warn_count;
    }

    public String getWarn_hwinit_val() {
        return warn_hwinit_val;
    }

    public void setWarn_hwinit_val(String warn_hwinit_val) {
        this.warn_hwinit_val = warn_hwinit_val;
    }

    public String getWarn_fm_full_val() {
        return warn_fm_full_val;
    }

    public void setWarn_fm_full_val(String warn_fm_full_val) {
        this.warn_fm_full_val = warn_fm_full_val;
    }

    public String getWarn_ej_full_val() {
        return warn_ej_full_val;
    }

    public void setWarn_ej_full_val(String warn_ej_full_val) {
        this.warn_ej_full_val = warn_ej_full_val;
    }

    public String getErr_fm_status() {
        return err_fm_status;
    }

    public void setErr_fm_status(String err_fm_status) {
        this.err_fm_status = err_fm_status;
    }
}
