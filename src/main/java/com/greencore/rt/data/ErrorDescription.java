package com.greencore.rt.data;

public class ErrorDescription {
    String name;
    String description, errorCode;

    public ErrorDescription(String errorCode, String name , String description){
        this.errorCode = errorCode;
        this.name = name;
        this.description = description;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
