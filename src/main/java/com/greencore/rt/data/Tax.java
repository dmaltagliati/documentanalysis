package com.greencore.rt.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 10/10/2018.
 */
public class Tax {
    private String gross;
    private String tax;
    @SerializedName("vatcode")
    private String vatCode;
    @SerializedName("vatvalue")
    private String vatValue;
    private double itemsSum;

    public String getGross() {
        return gross;
    }

    public void setGross(String gross) {
        this.gross = gross;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getVatCode() {
        if (vatCode == null)
            vatCode = "";
        return vatCode;
    }

    public void setVatCode(String vatCode) {
        this.vatCode = vatCode;
    }

    public String getVatValue() {
        return vatValue;
    }

    public void setVatValue(String vatValue) {
        this.vatValue = vatValue;
    }

    public double getItemsSum() {
        return itemsSum;
    }

    public void setItemsSum(double itemsSum) {
        this.itemsSum = itemsSum;
    }
}
