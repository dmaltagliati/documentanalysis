package com.greencore.rt.data;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by User on 10/10/2018.
 */
public class Document {
    private String amount;
    @SerializedName("businessname")
    private String businessName;
    @SerializedName("date_time_doc")
    private String dateTime;
    @SerializedName("docnumber")
    private String number;
    @SerializedName("doctype")
    private String type;
    @SerializedName("dtime")
    private String timeStamp;
    private String errSignature;
    @SerializedName("err_grand_total")
    private String errGrandTotal;
    @SerializedName("err_number")
    private String errNumber;
    @SerializedName("err_znumber")
    private String errZNumber;
    @SerializedName("fiscalcode")
    private String fiscalCode;
    @SerializedName("fiscaloperator")
    private String fiscalOperator;
    @SerializedName("qr_addinfo")
    private String qrAddInfo;
    private String shaFiscalData;
    private String signature;
    @SerializedName("type_signature_id")
    private String typeSignatureId;
    @SerializedName("vatcode")
    private String vatCode;
    private double itemsSum;
    private double taxesSum;
    private String cashuuid;
    private String docznumber;
    private String bussinessname;
    private String prevSignature;
    private double grandTotal;
    private int referenceClosurenumber;
    private int referenceDocnumber;
    private String referenceDtime;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getErrSignature() {
        return errSignature;
    }

    public void setErrSignature(String errSignature) {
        this.errSignature = errSignature;
    }

    public String getErrGrandTotal() {
        return errGrandTotal;
    }

    public void setErrGrandTotal(String errGrandTotal) {
        this.errGrandTotal = errGrandTotal;
    }

    public String getErrNumber() {
        return errNumber;
    }

    public void setErrNumber(String errNumber) {
        this.errNumber = errNumber;
    }

    public String getErrZNumber() {
        return errZNumber;
    }

    public void setErrZNumber(String errZNumber) {
        this.errZNumber = errZNumber;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public String getFiscalOperator() {
        return fiscalOperator;
    }

    public void setFiscalOperator(String fiscalOperator) {
        this.fiscalOperator = fiscalOperator;
    }

    public String getQrAddInfo() {
        return qrAddInfo;
    }

    public void setQrAddInfo(String qrAddInfo) {
        this.qrAddInfo = qrAddInfo;
    }

    public String getShaFiscalData() {
        return shaFiscalData;
    }

    public void setShaFiscalData(String shaFiscalData) {
        this.shaFiscalData = shaFiscalData;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getTypeSignatureId() {
        return typeSignatureId;
    }

    public void setTypeSignatureId(String typeSignatureId) {
        this.typeSignatureId = typeSignatureId;
    }

    public String getVatCode() {
        return vatCode;
    }

    public void setVatCode(String vatCode) {
        this.vatCode = vatCode;
    }

    public double getItemsSum() {
        return itemsSum;
    }

    public void setItemsSum(double itemsSum) {
        this.itemsSum = itemsSum;
    }

    public double getTaxesSum() {
        return taxesSum;
    }

    public void setTaxesSum(double taxesSum) {
        this.taxesSum = taxesSum;
    }

    public String getCashuuid() {
        return cashuuid;
    }

    public void setCashuuid(String cashuuid) {
        this.cashuuid = cashuuid;
    }

    public String getDocznumber() {
        return docznumber;
    }

    public void setDocznumber(String docznumber) {
        this.docznumber = docznumber;
    }

    public String getBussinessname() {
        return bussinessname;
    }

    public void setBussinessname(String bussinessname) {
        this.bussinessname = bussinessname;
    }

    public String getPrevSignature() {
        return prevSignature;
    }

    public void setPrevSignature(String prevSignature) {
        this.prevSignature = prevSignature;
    }

    public double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(double grandTotal) {
        this.grandTotal = grandTotal;
    }

    public int getReferenceClosurenumber() {
        return referenceClosurenumber;
    }

    public void setReferenceClosurenumber(int referenceClosurenumber) {
        this.referenceClosurenumber = referenceClosurenumber;
    }

    public int getReferenceDocnumber() {
        return referenceDocnumber;
    }

    public void setReferenceDocnumber(int referenceDocnumber) {
        this.referenceDocnumber = referenceDocnumber;
    }

    public String getReferenceDtime() {
        return referenceDtime;
    }

    public void setReferenceDtime(String referenceDtime) {
        this.referenceDtime = referenceDtime;
    }
}
