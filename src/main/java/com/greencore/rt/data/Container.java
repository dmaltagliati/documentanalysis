package com.greencore.rt.data;

public interface Container {
    public Data getData();

    public void setData(Data data) ;

}
