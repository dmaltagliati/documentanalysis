package com.greencore.rt.data;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.File;
import java.util.Date;

public class Log4JUtil {
    public static Logger get(Class theClass) {
        return Logger.getLogger(theClass);
    }

    public static boolean init(){
        System.out.println(" ENTER log4jInit()");
        String m_strConfigurationFileName;

        //m_strConfigurationFileName = "./"+Misc.PATH_CONF+"/" + Version.getName() + "_Log4j.properties";
        m_strConfigurationFileName = "./conf/"+"RTDocAnalysis_log4j.properties";


        System.out.println(" loading >" + m_strConfigurationFileName + "< config file");
        File file = new File(m_strConfigurationFileName);

        if (!file.exists()) {
            System.out.println(m_strConfigurationFileName + " not found.");
            return false;
        }
        try {

            PropertyConfigurator.configure(m_strConfigurationFileName);

        } catch (Throwable e) {
            System.out.println(new Date() + " [" + System.currentTimeMillis() + "] " + "Exception opening file \"" + m_strConfigurationFileName + "\"");
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
