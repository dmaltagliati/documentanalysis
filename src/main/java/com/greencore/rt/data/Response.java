package com.greencore.rt.data;


public class Response {

    String signedDigest ;
    String publKey;
    String responseCode;
    String responseDesc;
    String[] responseSubCode;
    ResponseError responseErr;
    boolean fromAdapter = true;
    String doctype;

    public String getSignedDigest() {
        return signedDigest;
    }

    public void setSignedDigest(String signedDigest) {
        this.signedDigest = signedDigest;
    }

    public String getPublKey() {
        return publKey;
    }

    public void setPublKey(String publKey) {
        this.publKey = publKey;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseDesc() {
        return responseDesc;
    }

    public void setResponseDesc(String responseDesc) {
        this.responseDesc = responseDesc;
    }

    public String[] getResponseSubCode() {
        return responseSubCode;
    }

    public void setResponseSubCode(String[] responseSubCode) {
        this.responseSubCode = responseSubCode;
    }

    public ResponseError getResponseErr() {
        return responseErr;
    }

    public void setResponseErr(ResponseError responseErr) {
        this.responseErr = responseErr;
    }

    public boolean isFromAdapter() {
        return fromAdapter;
    }

    public void setFromAdapter(boolean fromAdapter) {
        this.fromAdapter = fromAdapter;
    }

    public String getDoctype() {
        return doctype;
    }

    public void setDoctype(String doctype) {
        this.doctype = doctype;
    }
}
